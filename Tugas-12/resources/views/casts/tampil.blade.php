@extends('layouts.master')
@section('title')
    Halaman Tampil Cast
@endsection

@section('sub-title')
    Cast
@endsection

@section('content')
    <a href="/cast/create" class="btn btn-primary my-3">Tambah</a>

    <table class="table">
        <thead>
            <tr>
                <th scope="col">#</th>
                <th scope="col">Nama</th>
                <th scope="col">Umur</th>
                <th scope="col">Bio</th>
                <th scope="col">Action</th>
            </tr>
        </thead>
        <tbody>
            @forelse ($cast as $key => $value)
                <tr>
                    <th scope="row">{{$key + 1}}</th>
                    <td>{{$value->nama}}</td>
                    <td>{{$value->umur}}</td>
                    <td>{{$value->bio}}</td>
                    <td>
                        <form action="/cast/{{$value->id}}" method="post">
                        <a href="/cast/{{$value->id}}" class="btn btn-primary btn-sm">Detail</a>
                        <a href="/cast/{{$value->id}}/edit" class="btn btn-warning btn-sm">Edit</a>
                            @csrf
                            @method('delete')
                            <input type="submit" class="btn btn-danger btn-sm" value="Delete">
                        </form>
                    </td>
                </tr>
            @empty
                <tr>
                    <td>Data cast kosong, silakan menambah data.</td>
                </tr>
            @endforelse

 
        </tbody>
    </table>

@endsection