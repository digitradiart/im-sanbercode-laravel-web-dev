@extends('layouts.master')
@section('title')
    Halaman Edit Cast
@endsection

@section('sub-title')
    Cast
@endsection

@section('content')
    <form action="/cast/{{$cast->id}}" method="post">
        @csrf
        @method('put')
        <div class="form-group">
            <label>Cast Name</label>
            <input type="text" class="form-control @error('nama') is-invalid @enderror" name="nama" value="{{$cast->nama}}">
        </div>

        @error('nama')
            <div class="alert alert-danger" role="alert">
            {{$message}}
            </div>
        @enderror

        <div class="form-group">
            <label>Cast Age</label>
            <input type="number" class="form-control @error('umur') is-invalid @enderror" name="umur" value="{{$cast->umur}}">
        </div>

        @error('umur')
            <div class="alert alert-danger" role="alert">
            {{$message}}
            </div>
        @enderror

        <div class="form-group">
            <label>Cast Bio</label>
            <input type="text" class="form-control @error('bio') is-invalid @enderror" name="bio" value="{{$cast->bio}}">
        </div>

        @error('bio')
            <div class="alert alert-danger" role="alert">
            {{$message}}
            </div>
        @enderror

        <button type="submit" class="btn btn-primary">Submit</button>
    </form>
@endsection