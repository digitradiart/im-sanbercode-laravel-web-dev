@extends('layouts.master')

@section('title')
    Halaman Home
@endsection

@section('sub-title')
    Home
@endsection

@section('content')
    <h1>Selamat Datang {{$namaDepan}} {{$namaBelakang}}!</h1>
    <h2>
      Terima kasih telah bergabung di SanberBook, social media kita bersama!
    </h2>
@endsection