<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class CastController extends Controller
{
    public function create() 
    {
        return view('casts.tambah');
    }

    public function store(Request $request)
    {
        // validasi
        $request->validate([
            'nama' => 'required|max:255|min:5',
            'umur' => 'required',
            'bio' => 'required'
        ]);

        // insert data ke db
        DB::table('cast')->insert([
            'nama' => $request->input('nama'),
            'umur' => $request->input('umur'),
            'bio' => $request->input('bio')
        ]);

        // return redirect
        return redirect('/cast');
    }


    public function index()
    {
        $cast = DB::table('cast')->get();
        // dd($cast);
        return view('casts.tampil', ['cast'=>$cast]);
    }

    public function show($id)
    {
        $cast = DB::table('cast')->find($id);
        // dd($cast);
        return view('casts.detail', ['cast'=>$cast]);
    }

    public function edit($id)
    {
        $cast = DB::table('cast')->find($id);
        // dd($cast);
        return view('casts.edit', ['cast'=>$cast]);
    }
    
    public function update($id, Request $request)
    {
        // validasi
        $request->validate([
            'nama' => 'required|max:255|min:5',
            'umur' => 'required',
            'bio' => 'required'
        ]);

        // insert data ke db
        DB::table('cast')
            ->where('id', $id)
            ->update([
                'nama' => $request->input('nama'),
                'umur' => $request->input('umur'),
                'bio' => $request->input('bio')
            ]);

        // return redirect
        return redirect('/cast');
    }

    public function destroy($id)
    {
        DB::table('cast')
            ->where('id', $id)
            ->delete();

        return redirect('/cast');
    }
}
